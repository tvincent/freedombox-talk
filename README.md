# A FreedomBox presentation

This presentation was given during a Debian meetup in Nantes in December 2019.
Its aim was to introduce the project, how it interacts with Debian, and do a
quick demonstration on a small computer.

## How to build

The source of this presentation is written in [Pris](https://github.com/ruuda/pris), a language for designing slides.
Follow its [build instructions](https://github.com/ruuda/pris#building) to have a working `pris` binary.

Then, clone this repo:

```
git clone https://salsa.debian.org/tvincent/freedombox-talk.git
```

and build the slides:

```
[path/to]/pris freedombox_demo.pris
```

You will obtain `freedombox_demo.pdf` as a result.