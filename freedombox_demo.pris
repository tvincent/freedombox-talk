author = "tvincent"
title = "FreedomBox"
subtitle = "Mettez un papillon dans votre serveur"

color_blue = #1771CF
color_grey = #646464
color_white = #FFFFFF
color_black = #000000
color_debian = #d70a53

first_item = 0.3h
second_item = 0.42h
third_item = 0.54h
fourth_item = 0.66h
fifth_item = 0.78h

font_family = "Lato"
font_style = "Regular"
font_size = 0.07h

slide_center = (0.5w, 0.5h)
left_anchor = 0.05w

layout = function() {
    color = color_blue
    d = 0.04h
    put fill_rectangle((1w,d)) at (0w, 1h-d)
    color = #ffffff
    font_size = 0.03h
    put t(author) at (0.01w, 0.99h)
    put t(title) at (0.87w, 0.99h)
}

section = function(section_title) {   
    color = color_white
    text_align = "center"
    font_size = 0.1h
    font_style = "Bold"
    put t(section_title) at slide_center
}

slide_title = function(title) {
    color = color_blue
    line_width = 0.002w
    put t(title) at (left_anchor, 0.1h)
    put line ((0.9w, 0h)) at (left_anchor, 0.12h)
}

link = function(text, uri)
{
  color = color_blue
  line_width = 0.07em
  label = t(text)
  put label
  put hyperlink(uri, label.size) at (0em, 0.3em) + label.offset
  put line((label.width, 0em)) at (0em, 0.1em)
}

{
    background_color = color_blue
    put fit(image("images/FreedomBox-logo-grayscale-negative.svg"), (1h, 1h)) at (0.22w,0.1h)
    color = color_white
    text_align = "center"
    font_size = 0.05h
    put t(subtitle) at (0.5w, 0.8h)
}

{
    background_color = color_blue
    put section("C’est qui lui ?")
}

{
    put slide_title("tvincent")
    put t("• Contributeur Debian depuis 2011") at (left_anchor, first_item)
    put t("• Développeur Debian (non up.) depuis 2015") at (left_anchor, second_item)
    put t("• Contributeur occasionnel Freedombox depuis…\ntout à l’heure") at (left_anchor, third_item)
}

{
    background_color = color_blue
    put section("Qu’est-ce que c’est ?")
}

{
    put slide_title("Objectif")
    put t("Un serveur :") at (left_anchor, first_item)
    put t("• simple à administrer") at (left_anchor, second_item)
    put t("• hébergeable à domicile") at (left_anchor, third_item)
    put t("• pour machines peu puissantes")  at (left_anchor, fourth_item)
    put t("Objectifs similaires à ceux de ") ~ link("YUNOHOST", "https://yunohost.org") at (left_anchor, 0.85h)
}

{
    put slide_title("Pour quoi faire ?")
    put t("• Navigation privée") at (left_anchor, first_item)
    put t("• Communications sécurisées") at (left_anchor, second_item)
    put t("• Activisme en sécurité") at (left_anchor, third_item)
}

{
    put slide_title("Navigation privée")
    put t("Protection contre le pistage : Tor, Dynamic DNS,…") at (left_anchor, first_item)
    put t("Protection contre la censure : OpenVPN (serveur)") at (left_anchor, second_item)
    put t("Bloqueur de pub et traqueurs : Privoxy") at (left_anchor, third_item)
    put t("Moteur de recherche : Searx") at (left_anchor, fourth_item)
}

{
    put slide_title("Communications sécurisées")
    put t("Chat : XMPP, IRC, Matrix") at (left_anchor, first_item)
    put t("Partage de fichiers : Deluge, Transmission, …") at (left_anchor, second_item)
    put t("Communication vocale : Mumble") at (left_anchor, third_item)
    put t("Courriel : RoundCube (client seulement)") at (left_anchor, fourth_item)
}

{
    background_color = color_blue
    put section("Qui fait ça ?")
}

{
    put slide_title("Historique")
    put t("• 2010 - \"Freedom in the Cloud\" (") ~ link("Peertube","https://peertube.fr/videos/watch/2d7f1421-2e64-4756-9d90-c75860e3c378") ~t(", ") ~ link("Youtube", "https://www.youtube.com/watch?v=QOEMv0S8AcA") ~t(")") at (left_anchor, first_item)
    put t("• 2010-2019 - ???") at (left_anchor, second_item)
    put t("• 2019 - FreedomBox stable v19.1") at (left_anchor, third_item)
}

{
    put slide_title("Contributeurs")
    put t("• ") ~ link("FreedomBox Foundation", "https://freedomboxfoundation.org/") ~ t(" (US Non-profit)") at (left_anchor, first_item)
    put t("• ~60 développeurs") at (left_anchor, second_item)
    put t("• ~60 traducteurs") at (left_anchor, third_item)
}

{
    background_color = color_blue
    put section("Comment ça marche ?")
}

{
    put slide_title("Comment ça marche ?")
    put t("• Liste de paquets Debian préselectionnés") at (left_anchor, first_item)
    put t("• Plinth, interface d’admin, aussi dans Debian") at (left_anchor, second_item)
    put t("Mais du coup, qu’est-ce qui est spécifique\nà FreedomBox ?") at (left_anchor, 0.65h)
}

{
    background_color = color_debian
    put fit(image("images/openlogo-nd-white.svg"), (0.75w, 0.75h)) at (0.33w, 0.05h)
    color = color_white
    text_align = "center"
    font_size = 0.1h
    font_style = "Bold"
    put t("Ce n’est que du Debian !") at (0.5w, 0.9h)
}

{
    put slide_title("Debian Pure Blends")
    put t("Variantes de Debian destinées à des groupes\naux besoins particuliers.") at (left_anchor, 0.3h)
    put t("Leur but est de simplifier l’installation et\nl’administration pour ces groupes.") at (left_anchor, 0.6h)
}

{
    put slide_title("Quelques blends")
    put t("• ") ~ link("Debian Astro", "https://blends.debian.org/astro/") at (left_anchor, first_item)
    put t("• ") ~ link("Debian Med", "https://www.debian.org/devel/debian-med/") at (left_anchor, second_item)
    put t("• ") ~ link("Debian Edu", "https://wiki.debian.org/DebianEdu/") at (left_anchor, third_item)
    put t("• ") ~ link("Debian Games", "https://wiki.debian.org/Games") at (left_anchor, fourth_item)
    put link("Liste complète", "https://blends.debian.org") at (left_anchor, 0.85h)

}

{
    background_color = color_blue
    put section("Et ça tourne sur quoi ?")
}

{
    put slide_title("Matériel pris en charge")
    put t("• Pioneer edition (officielle)") at (left_anchor, first_item)
    put t("• Olimex OLinuXino Lime 1, 2") at (left_anchor, second_item)
    put t("• Raspberry pi 2, 3, (4 ?)") at (left_anchor, third_item)
    put t("• Images de VM") at (left_anchor, fourth_item)
    put t("• Tout ce qui peut faire tourner Debian") at (left_anchor, fifth_item)
}

{
    background_color = color_blue
    put section("Démo !")
}

{
    put slide_title("Démo")
    put t("• Olimex OLinuXino Lime (brique Internet)") at (left_anchor, first_item)
    put t("• FreedomBox stable") at (left_anchor, second_item)
    put t("• ") ~ link("tvincent-freedombox-demo.debian.net", "https://tvincent-freedombox-demo.debian.net") at (left_anchor, third_item)
    put t("• Démo officielle en ligne : ") ~ link("freedombox.org/demo/", "https://www.freedombox.org/demo/") at (left_anchor, fourth_item)
}

{
    background_color = color_blue
    put section("Contribuez !")
}

{
    put slide_title("Comment contribuer")
    put t("• ") ~ link("Liste de discussion", "https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/freedombox-discuss") at (left_anchor, first_item)
    put t("• ") ~ link("Contributor Invites", "https://discuss.freedombox.org/c/contributor-invites") at (left_anchor, second_item)
    put t("• ") ~ link("Salsa", "https://salsa.debian.org/freedombox-team") at (left_anchor, third_item)
    put t("• ") ~ link("Traduction", "https://hosted.weblate.org/projects/freedombox/plinth/fr/") at (left_anchor, fourth_item)
    put t("• Contribuer à Debian en général !") at (left_anchor, fifth_item)
}

{
    background_color = color_blue
    put section("Questions ?")
}

{
    background_color = color_blue
    put section("Merci de votre attention")
}

{
    put slide_title("Liens")
    put t("• ") ~ link("FreedomBox", "https://www.freedombox.org/") at (left_anchor, first_item)
    put t("• ") ~ link("FreedomBox Foundation", "https://freedomboxfoundation.org/") at (left_anchor, second_item)
    put t("• ") ~ link("Sources de cette présentation", "https://salsa.debian.org/tvincent/freedombox-talk") at (left_anchor, third_item)
}
